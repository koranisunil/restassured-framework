package Driver_package;

import java.io.IOException;

import Test_package.Get_TC1;
import Test_package.Patch_TC1;
import Test_package.Post_TC1;
import Test_package.Put_TC1;

public class Driver_class {

	public static void main(String[] args) throws IOException {

		Post_TC1.executor();
		Put_TC1.executor();
		Patch_TC1.executor();
		Get_TC1.executor();
	}

}
