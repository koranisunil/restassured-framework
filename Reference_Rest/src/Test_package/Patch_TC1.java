package Test_package;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import Endpoints.Patch_endpoint;
import Parse_req.Patch_Reqbody;
import Repository_Request.Patch_request_repository;
import Utility_common_methods.Handle_API_logs;
import Utility_common_methods.Handle_directory;

public class Patch_TC1 extends Common_method_handle_API {
	@Test
	public static void executor() throws IOException {
		File log_dir= Handle_directory.Create_log_directory("Patch_TC1_logs");
		String requestBody = Patch_request_repository.Patch_request_TC1();

		String endpoint = Patch_endpoint.Patch_endpoint_TC1();

		for (int i = 0; i < 5; i++) {
			int statusCode = Patch_statusCode(requestBody, endpoint);
			System.out.println(statusCode);
			if (statusCode == 200) {

				String responseBody = Patch_responseBody(requestBody, endpoint);
				System.out.println(responseBody);
				Patch_Reqbody.validator(requestBody, responseBody);
				Handle_API_logs.evidence_creator(log_dir, "Patch_TC1", endpoint, requestBody, responseBody);
				break;

			} else {
				System.out.println("Expected Status code not found hence retrying");
			}
		}
	}
}
