package Test_package;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import Endpoints.Post_endpoint;
import Parse_req.Post_Reqbody;
import Repository_Request.Post_request_repository;
import Utility_common_methods.Handle_API_logs;
import Utility_common_methods.Handle_directory;

public class Post_TC1 extends Common_method_handle_API {
	@Test
	public static void executor() throws IOException {
		File log_dir = Handle_directory.Create_log_directory("Post_TC1_logs");
		System.out.println(log_dir);
		String requestBody = Post_request_repository.Post_request_TC1();

		String endpoint = Post_endpoint.Post_endpoint_TC1();
		for (int i = 0; i < 5; i++) {

			int statusCode = Post_statusCode(requestBody, endpoint);
			System.out.println(statusCode);
			if (statusCode == 201) {

				String responseBody = Post_responseBody(requestBody, endpoint);
				System.out.println(responseBody);
				Handle_API_logs.evidence_creator(log_dir, "Post_TC1", endpoint, requestBody, responseBody);
				Post_Reqbody.validator(requestBody, responseBody);
				break;

			} else {
				System.out.println("Expected Status code not found hence retrying");
			}
		}
	}
}
