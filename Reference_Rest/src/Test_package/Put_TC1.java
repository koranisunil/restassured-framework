package Test_package;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import Endpoints.Put_endpoint;
import Parse_req.Put_ReqBody;
import Repository_Request.Put_request_repository;
import Utility_common_methods.Handle_API_logs;
import Utility_common_methods.Handle_directory;


public class Put_TC1 extends Common_method_handle_API {
	@Test
	public static void executor() throws IOException {
		File log_dir= Handle_directory.Create_log_directory("Put_TC1_logs");
		String requestBody = Put_request_repository.Put_request_TC1();
		String endpoint = Put_endpoint.Put_endpoint_TC1();
		for (int i = 0; i < 5; i++) {
			int statusCode = Put_statusCode(requestBody, endpoint);
			System.out.println(statusCode);
			if (statusCode == 200) {

				String responseBody = Put_responseBody(requestBody, endpoint);
				System.out.println(responseBody);
				Handle_API_logs.evidence_creator(log_dir, "Put_TC1", endpoint, requestBody, responseBody);
				Put_ReqBody.validator(requestBody, responseBody);
				break;
				
			} else {
				System.out.println("Expected Status code not found hence retrying");
			}
		}
	}
}
