package Utility_common_methods;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_API_logs {
	public static void evidence_creator(File dir_name, String File_name, String endpoint, String requestBody,
			String responseBody) throws IOException {
		File newFile = new File(dir_name + "\\" + File_name + ".txt");

		FileWriter datawriter = new FileWriter(newFile);
		datawriter.write("Endpoint is : " + endpoint + "\n\n");
		datawriter.write("Request Body is : " + requestBody + "\n\n");
		datawriter.write("Response Body is : " + responseBody);
		datawriter.close();
		System.out.println("To save request & responseBody we have created a new file named : " + newFile.getName());

	}

}
