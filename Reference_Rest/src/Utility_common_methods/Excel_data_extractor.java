package Utility_common_methods;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_data_extractor {

	public static ArrayList<String> Excel_data_reader(String Filename, String Sheetname, String TC_name)
			throws IOException {

		ArrayList<String> Arraydata = new ArrayList<String>();
		String project_dir = System.getProperty("user.dir"); // fetch the current project directory

		// Step 1 create the object of file input stream to locate the data file
		FileInputStream FIS = new FileInputStream(project_dir + "//Data_files//" + Filename + ".xlsx");

		// Step 2 Create the XSSFWorkBook object to open the excel file
		XSSFWorkbook wb = new XSSFWorkbook(FIS);

		// Step 3 Fetch the number of sheets available in the excel file
		int count = wb.getNumberOfSheets();

		// Step 4 Access the sheet as per the input sheet name
		for (int i = 0; i < count; i++) {
			String sheetname = wb.getSheetName(i);

			if (sheetname.equals(Sheetname)) {
				System.out.println(sheetname);
				XSSFSheet sheet = wb.getSheetAt(i);
				Iterator<Row> row = sheet.iterator();
				row.next();
				while (row.hasNext()) {
					Row datarow = row.next();
					String TCname = datarow.getCell(0).getStringCellValue();
					if (TCname.equals(TC_name)) {
						Iterator<Cell> cellvalues = datarow.iterator();
						while (cellvalues.hasNext()) {
							String Test_data = cellvalues.next().getStringCellValue();
							//System.out.println(Test_data);
							Arraydata.add(Test_data);
						}

					}

				}
				break;
			}

		}
		wb.close();
		return Arraydata;
	}

}
